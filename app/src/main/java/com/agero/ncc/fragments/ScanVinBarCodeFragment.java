package com.agero.ncc.fragments;


import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.activities.ScannerActivity;
import com.agero.ncc.app.NCCApplication;

import com.agero.ncc.model.CapturedVinInfo;
import com.agero.ncc.utils.DateTimeUtils;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import durdinapps.rxfirebase2.RxFirebaseDatabase;

public class ScanVinBarCodeFragment extends BaseFragment implements HomeActivity.ToolbarSaveListener {

    HomeActivity mHomeActivity;
    private static final int REQUEST_VIN = 1;
    @BindView(R.id.edittext_vin_no)
    EditText mEdittextVinNo;
    private boolean isPermissionDenied;
    private String mDispatchId;
    private DatabaseReference mVinReference;
    private UserError mUserError;

    public ScanVinBarCodeFragment() {
        // Intentionally empty
    }

    public static ScanVinBarCodeFragment newInstance(String dispatcherId) {
        ScanVinBarCodeFragment fragment = new ScanVinBarCodeFragment();
        Bundle bundle = new Bundle();
        bundle.putString(NccConstants.DISPATCH_REQUEST_NUMBER, dispatcherId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);
        View view = inflater.inflate(R.layout.fragment_scanvin, fragment_content, true);
        ButterKnife.bind(this, view);
        NCCApplication.getContext().getComponent().inject(this);

        if (getArguments() != null) {
            mDispatchId = getArguments().getString(NccConstants.DISPATCH_REQUEST_NUMBER);
        }
        mUserError = new UserError();
        mHomeActivity = (HomeActivity) getActivity();
        mHomeActivity.hideBottomBar();
        mHomeActivity.setOnToolbarSaveListener(this);
        mHomeActivity.showToolbar(getString(R.string.title_scanvin));
        mHomeActivity.saveDisable();
        mEdittextVinNo.addTextChangedListener(textWatcher);
        initFirebase();
        return superView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isPermissionDenied) {
            isPermissionDenied = false;
            showPermissionDeniedDialog();
        }
    }


    @OnClick({R.id.edittext_vin_no, R.id.image_vin_cam})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_vin_cam:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!Dexter.isRequestOngoing()) {
                        Dexter.checkPermission(permissionListener, Manifest.permission.CAMERA);
                    }
                } else {
                    startActivityForResult(new Intent(getActivity(), ScannerActivity.class), REQUEST_VIN);
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_VIN && resultCode == Activity.RESULT_OK) {

            if (mEdittextVinNo != null) {
                mEdittextVinNo.setText(data.getStringExtra("Result"));
                mEditor.putString(NccConstants.JOBDETAIL_SCAN_VIN, data.getStringExtra("Result")).commit();
            }
        }
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (editable.length() == 17) {
                mHomeActivity.saveEnable();
            } else {
                mHomeActivity.saveDisable();
            }
        }
    };

    PermissionListener permissionListener = new PermissionListener() {
        @Override
        public void onPermissionGranted(PermissionGrantedResponse response) {
            startActivityForResult(new Intent(getActivity(), ScannerActivity.class), REQUEST_VIN);

        }

        @Override
        public void onPermissionDenied(PermissionDeniedResponse response) {
            if (response.isPermanentlyDenied()) {
                isPermissionDenied = true;
            }
        }

        @Override
        public void onPermissionRationaleShouldBeShown(PermissionRequest permission,
                                                       PermissionToken token) {
            token.continuePermissionRequest();
        }
    };

    private void initFirebase() {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        mVinReference = firebaseDatabase.getReference("ActiveJobs/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "") + "/" + mDispatchId + "/capturedVinInfo");
        //mVinReference.keepSynced(true);
    }

    private void showPermissionDeniedDialog() {
        AlertDialogFragment alert = AlertDialogFragment.newOkDialog(getResources().getString(R.string.permission_denied_title), getResources().getString(R.string.permission_denied_camera_message));
        alert.setListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getActivity().startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.fromParts("package", getActivity().getPackageName(), null)));
                dialogInterface.dismiss();
            }
        });
        alert.show(getFragmentManager().beginTransaction(), ScanVinBarCodeFragment.class.getClass().getCanonicalName());
    }

    @Override
    public void onSave() {
        if (Utils.isNetworkAvailable()) {
            showProgress();
            TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {
                        CapturedVinInfo capturedVinInfo = new CapturedVinInfo(DateTimeUtils.getUtcTimeFormat(),NccConstants.SUB_SOURCE,mEdittextVinNo.getText().toString());

                        HashMap<String, String> extraDatas = new HashMap<>();
                        extraDatas.put("dispatch_id", mDispatchId);
                        extraDatas.put("json", new Gson().toJson(capturedVinInfo));
                        mHomeActivity.mintlogEventExtraData("Scan Vin Barcode", extraDatas);

                        RxFirebaseDatabase.setValue(mVinReference, capturedVinInfo).subscribe(() -> {
                            hideProgress();
                            if(isAdded()){
                                getActivity().onBackPressed();
                            }
                        }, throwable -> {
                            hideProgress();
                        });
                    }
                }

                @Override
                public void onRefreshFailure() {
                    hideProgress();
                    if (isAdded()) {
                        mHomeActivity.tokenRefreshFailed();
                    }
                }
            });
        } else {
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }
}
