package com.agero.ncc.fragments;


import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.agero.ncc.BuildConfig;
import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.activities.WelcomeActivity;
import com.agero.ncc.adapter.ActiveJobsAdapter;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.driver.fragments.JobDetailsFragment;
import com.agero.ncc.model.Facility;
import com.agero.ncc.model.JobDetail;
import com.agero.ncc.model.Profile;
import com.agero.ncc.utils.DateTimeUtils;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.zendesk.logger.Logger;
import com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ui.ContactZendeskActivity;
import com.zendesk.sdk.model.access.AnonymousIdentity;
import com.zendesk.sdk.model.access.Identity;
import com.zendesk.sdk.model.access.JwtIdentity;
import com.zendesk.sdk.model.request.CustomField;
import com.zendesk.sdk.network.impl.ZendeskConfig;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import timber.log.Timber;

public class AccountFragment extends BaseFragment implements HomeActivity.OnDutyToolbarListener {

    public static final int REQUEST_SIGNATURE_HINT_SCREEN = 1;
    HomeActivity mHomeActivity;
    @BindView(R.id.text_label_help)
    TextView mTextLabelHelp;
    @BindView(R.id.text_label_legal)
    TextView mTextLabelLegal;
    @BindView(R.id.text_company_location)
    TextView mTextCompany;
    @BindView(R.id.text_firstname)
    TextView mTextFirstname;
    @BindView(R.id.text_profile_info)
    TextView mTextProfileInfo;
    @BindView(R.id.image_photo)
    CircleImageView mImagePhoto;
    Profile mProfile;
    @BindView(R.id.text_default)
    TextView mTextRingtoneName;
    @BindView(R.id.text_lable_flname)
    TextView textLableFlname;


    UserError mUserError;
    Facility mFacility;
    @BindView(R.id.view_border_facility)
    View viewBorderFacility;
    @BindView(R.id.text_company)
    TextView textCompany;
    @BindView(R.id.view_border_settings)
    View viewBorderSettings;
    @BindView(R.id.text_label_alert_sound)
    TextView textLabelAlertSound;
    @BindView(R.id.view_border_alertsound)
    View viewBorderAlertsound;
    @BindView(R.id.view_border_help)
    View viewBorderHelp;
    @BindView(R.id.view_border_legal)
    View viewBorderLegal;
    @BindView(R.id.text_label_chat)
    TextView textLabelChat;
    @BindView(R.id.view_border_chat)
    View viewBorderChat;
    @BindView(R.id.text_label_signout)
    TextView textLabelSignout;
    @BindView(R.id.view_border_signout)
    View viewBorderSignout;
    @BindView(R.id.account)
    ConstraintLayout account;
    @BindView(R.id.constraint_fragment_account)
    ConstraintLayout mConstraintFragmentAccount;

    @BindView(R.id.text_label_version)
    TextView textLableVersion;
    private int serviceUnFinished = 0;
    private long oldRetryTime = 0;
    ValueEventListener profileValueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (isAdded()) {
                loadProfile(dataSnapshot);
                serviceUnFinished--;
                if (serviceUnFinished <= 0) {
                    hideProgress();
                }
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            hideProgress();
            if (isAdded()) {
                if (databaseError != null && databaseError.getMessage() != null) {
                    mHomeActivity.mintlogEvent("Account Screen User Profile Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
                }
                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                    getProfileDataFromFirebase();
                    oldRetryTime = System.currentTimeMillis();
                }
            }
        }
    };
    private DatabaseReference companyRef, myRef;
    ValueEventListener companyValueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (isAdded()) {
                loadFacility(dataSnapshot);
                serviceUnFinished--;
                if (serviceUnFinished <= 0) {
                    hideProgress();
                }
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            hideProgress();
            if (isAdded()) {
                if (databaseError != null && databaseError.getMessage() != null) {
                    mHomeActivity.mintlogEvent("Account Screen User Facility Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
                }
                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                    getFacilityDataFromFirebase();
                    oldRetryTime = System.currentTimeMillis();
                }
            }
        }
    };

    public AccountFragment() {
        // Intentionally empty
    }

    public static AccountFragment newInstance() {
        AccountFragment fragment = new AccountFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View view = inflater.inflate(R.layout.fragment_account, fragment_content, true);
        NCCApplication.getContext().getComponent().inject(this);
        ButterKnife.bind(this, view);
        mEditor = mPrefs.edit();
        mTextFirstname.setText(mPrefs.getString(NccConstants.USER_PROFILE, ""));
        mHomeActivity = (HomeActivity) getActivity();
        mHomeActivity.showBottomBar();
        mHomeActivity.showToolbar(getString(R.string.title_account));

        mUserError = new UserError();
        if (Utils.isNetworkAvailable()) {
            getProfileDataFromFirebase();
//            getFacilityDataFromFirebase();



        } else {
            mUserError.title = "";
            mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
        setAppVersion();
        if (getResources().getBoolean(R.bool.isTablet)) {
            openChildScreen(EditProfileFragment.newInstance(), getString(R.string.title_edit_profile));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Dexter.isRequestOngoing()) {
                Dexter.checkPermission(externalPermissionListener, Manifest.permission.READ_EXTERNAL_STORAGE);
            }
        } else {
            setSoundName(false);
        }
        return superView;
    }

    //    Runtime Permission listener to access external storage.
    PermissionListener externalPermissionListener = new PermissionListener() {
        @Override
        public void onPermissionGranted(PermissionGrantedResponse response) {
            if (isAdded()) {
                setSoundName(false);
            }
        }

        @Override
        public void onPermissionDenied(PermissionDeniedResponse response) {
            if (response.isPermanentlyDenied()) {
                setSoundName(true);
            }
        }

        @Override
        public void onPermissionRationaleShouldBeShown(PermissionRequest permission,
                                                       PermissionToken token) {
            token.continuePermissionRequest();
        }
    };

    private void getFacilityDataFromFirebase() {
        TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
            @Override
            public void onRefreshSuccess() {
                if (isAdded()) {
                    serviceUnFinished++;
                    FirebaseDatabase database = FirebaseDatabase.getInstance();

                    companyRef = database.getReference("Facility/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "")
                            + "/");
                    // companyRef.keepSynced(true);
                    companyRef.addValueEventListener(companyValueEventListener);


                }
            }

            @Override
            public void onRefreshFailure() {
                hideProgress();
                if (isAdded()) {
                    mHomeActivity.tokenRefreshFailed();
                }
            }
        });
    }

    private void getProfileDataFromFirebase() {

        showProgress();
        TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
            @Override
            public void onRefreshSuccess() {
                if (isAdded()) {
                    serviceUnFinished++;
                    FirebaseDatabase database = FirebaseDatabase.getInstance();
                    myRef = database.getReference("Users/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "")
                            + "/" + mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
                    //myRef.keepSynced(true);
                    myRef.addValueEventListener(profileValueEventListener);


                }
            }

            @Override
            public void onRefreshFailure() {
                hideProgress();
                if (isAdded()) {
                    mHomeActivity.tokenRefreshFailed();
                }
            }
        });

    }


    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent intent) {
        if (resultCode == Activity.RESULT_OK && requestCode == 5 && isAdded()) {
            Uri uri = intent.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
            if (uri != null) {
                Ringtone ringtone = RingtoneManager.getRingtone(getActivity(), uri);
                if (ringtone != null) {
                    mTextRingtoneName.setText(ringtone.getTitle(getActivity()));
                }
                mEditor.putString(NccConstants.PREF_KEY_NOTIFICATION_URI, uri.toString()).apply();
            }
        }
    }

    private void setSoundName(boolean isPermissionDenied) {
        if (!mPrefs.getString(NccConstants.PREF_KEY_NOTIFICATION_URI, "").isEmpty() && !isPermissionDenied) {
            Ringtone ringtone = RingtoneManager.getRingtone(getActivity(), Uri.parse(mPrefs.getString(NccConstants.PREF_KEY_NOTIFICATION_URI, "")));
            if (ringtone != null) {
                mTextRingtoneName.setText(ringtone.getTitle(getActivity()));
            }
        } else {
            mTextRingtoneName.setText(getString(R.string.settings_defaults));
        }
    }

    private void loadProfile(DataSnapshot dataSnapshot) {
        if (dataSnapshot.hasChildren()) {
            try {
                mTextFirstname.setText(mPrefs.getString(NccConstants.USER_PROFILE, ""));
                mProfile = dataSnapshot.getValue(Profile.class);
                HashMap<String, String> extraDatas = new HashMap<>();
                extraDatas.put("json", new Gson().toJson(mProfile));
                mHomeActivity.mintlogEventExtraData("Account Screen Profile", extraDatas);
                if (isAdded() && mProfile != null) {
                    textLableFlname.setVisibility(View.VISIBLE);
                    Logger.i("Identity", "Setting identity");
                    if(TextUtils.isEmpty(mProfile.getEmail())){
                        mProfile.setEmail(mPrefs.getString(NccConstants.SIGNIN_EMAILADDRESS,""));
                    }

                    String fullName = Utils.toCamelCase(mProfile.getFirstName() + " " + mProfile.getLastName());
                    Identity identity = new AnonymousIdentity.Builder().withNameIdentifier(fullName).withEmailIdentifier(mProfile.getEmail()).build();
                    ZendeskConfig.INSTANCE.setIdentity(identity);


                    mEditor.putString(NccConstants.USER_PROFILE, fullName).commit();
                    mEditor.putString(NccConstants.SIGNIN_USER_NAME, fullName);
                    mTextFirstname.setText(mPrefs.getString(NccConstants.USER_PROFILE, ""));
                    if (mProfile.getFirstName().length() > 0 && mProfile.getLastName().length() > 0) {
                        textLableFlname.setText(("" + mProfile.getFirstName().charAt(0) + mProfile.getLastName().charAt(0)).toUpperCase(Locale.ENGLISH));
                    }
               /* if (mProfile.getImageUrl() != null && !mProfile.getImageUrl().isEmpty()) {
                    Glide.with(getContext()).load(mProfile.getImageUrl()).listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model,
                                                    Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target,
                                                       DataSource dataSource, boolean isFirstResource) {
                            textLableFlname.setVisibility(View.GONE);
                            return false;
                        }
                    }).into(mImagePhoto);
                } else {*/
                    textLableFlname.setVisibility(View.VISIBLE);
                    if (mProfile.getFirstName().length() > 0 && mProfile.getLastName().length() > 0) {
                        textLableFlname.setText(("" + mProfile.getFirstName().charAt(0) + mProfile.getLastName().charAt(0)).toUpperCase(Locale.ENGLISH));
//                    }
                    }

                }
            } catch (Exception e) {
                mHomeActivity.mintLogException(e);
                e.printStackTrace();
            }
        }
    }


    private void loadFacility(DataSnapshot dataSnapshot) {
        if (dataSnapshot.hasChildren()) {
            try {
                mFacility = dataSnapshot.getValue(Facility.class);
                HashMap<String, String> extraDatas = new HashMap<>();
                extraDatas.put("json", new Gson().toJson(mFacility));
                mHomeActivity.mintlogEventExtraData("Account Screen Facility", extraDatas);
            } catch (Exception e) {
                mHomeActivity.mintLogException(e);
                e.printStackTrace();
            }
//            mTextCompany.setText(mFacility.getName());
        }
    }

    String eventAction = "";
    String eventCategory = NccConstants.FirebaseEventCategory.ACCOUNT;

    @OnClick({R.id.text_company, R.id.text_company_location, R.id.text_label_help, R.id.text_label_legal, R.id.text_label_signout,
            R.id.text_firstname, R.id.text_profile_info, R.id.image_photo, R.id.text_default, R.id.text_label_alert_sound, R.id.text_label_chat})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.text_firstname:
            case R.id.text_profile_info:
            case R.id.image_photo:
                eventAction = NccConstants.FirebaseEventAction.VIEW_PROFILE;
                openChildScreen(EditProfileFragment.newInstance(), getString(R.string.title_edit_profile));
//                mHomeActivity.pushWithAnimation(EditProfileFragment.newInstance(), getString(R.string.title_edit_profile));
                break;
            case R.id.text_company:
            case R.id.text_company_location:
                eventAction = NccConstants.FirebaseEventAction.VIEW_COMPANY;
                openChildScreen(MyCompanyFragment.newInstance(), getString(R.string.title_basic));
                break;
            case R.id.text_label_help:
                eventAction = NccConstants.FirebaseEventAction.VIEW_HELP;
//                mWelcomeActivity.push(new HelpFragment(),getString(R.string.title_help));
                //createZendeskTicket();
                openChildScreen(new ZendeskHelpFragment(), getString(R.string.title_help));

//                mHomeActivity.push(new ZendeskHelpFragment(), getString(R.string.title_help));
//                new SupportActivity.Builder()
//                        .showContactUsButton(true)
//                        .withCategoriesCollapsed(true).show(mHomeActivity);
                break;
            case R.id.text_label_legal:
                eventAction = NccConstants.FirebaseEventAction.VIEW_LEGAL;
                openChildScreen(LegalFragment.newInstance(), getString(R.string.title_legal));

//                mHomeActivity.push(LegalFragment.newInstance(), getString(R.string.title_legal));
                break;
            case R.id.text_label_chat:
                openChildScreen(ChatHistoryFragment.newInstance(), getString(R.string.title_chat_history));
//                mHomeActivity.push(ChatHistoryFragment.newInstance(), getString(R.string.title_chat_history));
                break;
            case R.id.text_label_signout:
                eventAction = NccConstants.FirebaseEventAction.SIGN_OUT;
                if (mHomeActivity.isLoggedInDriver() || mHomeActivity.isUserDispatcherAndDriver()) {
                    getJobsList();
                } else {
                    signOutAlertDialog(true, false);
                }
                break;
            case R.id.text_label_alert_sound:
            case R.id.text_default:
                eventAction = NccConstants.FirebaseEventAction.CHANGE_SOUND;
                if (isAdded()) {
                    Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
                    intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION);
                    intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, "Select Tone");
                    intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, Uri.parse(mPrefs.getString(NccConstants.PREF_KEY_NOTIFICATION_URI, "")));
                    startActivityForResult(intent, 5);
                }
                break;
        }
        mHomeActivity.firebaseLogEvent(eventCategory, eventAction);
    }

    private void getJobsList() {
        if (Utils.isNetworkAvailable()) {
            showProgress();
            TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {
                        hideProgress();
                        String vendorId = mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "");
                        String userId = mPrefs.getString(NccConstants.SIGNIN_USER_ID, "");
                        if (!TextUtils.isEmpty(vendorId) && !TextUtils.isEmpty(userId)) {
                            FirebaseDatabase database = FirebaseDatabase.getInstance();
                            showProgress();
                            database.getReference("ActiveJobs/").child(vendorId).orderByChild("dispatchAssignedToId").equalTo(userId).addListenerForSingleValueEvent(jobEventListner);
                        } else {
                            signOutAlertDialog(false, false);
                        }

                    }
                }

                @Override
                public void onRefreshFailure() {
                    hideProgress();
                    if (isAdded()) {
                        mHomeActivity.tokenRefreshFailed();
                    }
                }
            });
        } else {
            hideProgress();
            mUserError.title = "";
            mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }

    private void openChildScreen(Fragment fragment, String title) {
        if (getResources().getBoolean(R.bool.isTablet)) {
            mHomeActivity.push(fragment);
        } else {
            mHomeActivity.push(fragment, title);
        }
    }


    private void signOutAlertDialog(boolean isOnlyDispatcher, boolean hasJobs) {
        if (isAdded()) {
            String msg = getResources().getString(R.string.accounts_alert_messsage);
            if (!isOnlyDispatcher && hasJobs) {
                HashMap<String, String> extraDatas = new HashMap<>();
                extraDatas.put("hasJobs", "true");
                mHomeActivity.mintlogEventExtraData("Account Screen JobList", extraDatas);
                msg = getResources().getString(R.string.accounts_signout_alert_messsage_job);
            } else {
                HashMap<String, String> extraDatas = new HashMap<>();
                extraDatas.put("hasJobs", "false");
                mHomeActivity.mintlogEventExtraData("Account Screen SignOut JobList", extraDatas);
            }
            AlertDialogFragment alert = AlertDialogFragment.newDialog("", Html.fromHtml("<big>" + msg + "</big>")
                    , Html.fromHtml("<b>" + getString(R.string.accounts_alert_signout) + "</b>"), Html.fromHtml("<b>" + getString(R.string.accounts_alert_cancel) + "</b>"));

            alert.setListener(new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (isAdded()) {
                        if (i == -1) {
                            signOutUser(isOnlyDispatcher);
                        }
                        dialogInterface.dismiss();
                    }
                }
            });

            if (getFragmentManager() != null) {
                try {
                    alert.show(getFragmentManager().beginTransaction(), AccountFragment.class.getClass().getCanonicalName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void signOutUser(boolean isOnlyDispatcher) {
        if (isOnlyDispatcher) {
            mHomeActivity.clearSigninData();
        } else {
            goOffDuty();
        }
    }

    private ValueEventListener jobEventListner = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (isAdded()) {
                signOutAlertDialog(false, dataSnapshot.hasChildren());

            }


        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            hideProgress();
            if (isAdded()) {
                if (databaseError != null && databaseError.getMessage() != null) {
                    mHomeActivity.mintlogEvent("Driver Active Jobs List Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
                }
                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                    goOffDuty();
                    oldRetryTime = System.currentTimeMillis();
                }
            }
        }
    };


    private void goOffDuty() {
        if (Utils.isNetworkAvailable()) {
            showProgress();
            TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {
                        mHomeActivity.uploadDuty(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""), false, mPrefs.getString(NccConstants.EQUIPMENT_ID, ""), true);
                    }
                }

                @Override
                public void onRefreshFailure() {
                    hideProgress();
                    if (isAdded()) {
                        mHomeActivity.tokenRefreshFailed();
                    }
                }
            });
        } else {
            hideProgress();
            mUserError.title = "";
            mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }


    @Override
    public void onDutyCall() {

    }

    @Override
    public void onTextDuty() {

    }

    @Override
    public void offDutyCall() {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        removeValueEventListners();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        removeValueEventListners();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        removeValueEventListners();
    }

    private void removeValueEventListners() {
        try {
            if (myRef != null) {
                myRef.removeEventListener(profileValueEventListener);
            }
            if (companyRef != null) {
                companyRef.removeEventListener(companyValueEventListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAppVersion() {
        try {
            PackageInfo pInfo = mHomeActivity.getPackageManager().getPackageInfo(mHomeActivity.getPackageName(), 0);
            String version = pInfo.versionName;

            if ("prod".equalsIgnoreCase(BuildConfig.ENV)) {
                textLableVersion.setText(version);
            } else {
                textLableVersion.setText(version + " - " + BuildConfig.ENV);
            }

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
