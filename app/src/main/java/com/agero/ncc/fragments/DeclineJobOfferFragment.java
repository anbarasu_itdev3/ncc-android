package com.agero.ncc.fragments;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.utils.NccConstants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class DeclineJobOfferFragment extends DialogFragment {

    @BindView(R.id.text_decline_job_offer)
    TextView mTextDeclineJobOffer;
    @BindView(R.id.radioequipment_not_available)
    RadioButton mRadioequipmentNotAvailable;
    @BindView(R.id.radio_driver_not_available)
    RadioButton mRadioDriverNotAvailable;
    @BindView(R.id.radio_out_of_area)
    RadioButton mRadioOutOfArea;
    @BindView(R.id.radio_not_accepting_job)
    RadioButton mRadioNotAcceptingJob;
    @BindView(R.id.radio_other)
    RadioButton mRadioOther;
    @BindView(R.id.radioGroup)
    RadioGroup mRadioGroup;
    @BindView(R.id.edit_other)
    EditText mEditOther;
    @BindView(R.id.relative_layout)
    RelativeLayout mRelativeLayout;
    @BindView(R.id.text_ok)
    TextView mTextOk;
    @BindView(R.id.text_cancel)
    TextView mTextCancel;

    private String mSelectedReasonCode = "-1";


    public DeclineJobOfferFragment() {

    }

    public static DeclineJobOfferFragment newInstance() {
        DeclineJobOfferFragment fragment = new DeclineJobOfferFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_decline_job_offer, container);
        ButterKnife.bind(this, v);

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        mTextOk.setEnabled(false);
        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                if (radioGroup.getCheckedRadioButtonId() == R.id.radio_other) {
                    mEditOther.setVisibility(View.VISIBLE);
                    mRadioOther.setText("");
                } else {
                    mEditOther.setVisibility(View.GONE);
                    getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                    mRadioOther.setText(getResources().getString(R.string.decline_job_other));
                }
                mTextOk.setEnabled(true);
                mTextOk.setTextColor(getResources().getColor(R.color.jobdetail_background_button));
                mSelectedReasonCode = (String) ((RadioButton) radioGroup.findViewById(radioGroup.getCheckedRadioButtonId())).getTag();
            }
        });
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
    }
    @OnClick({R.id.text_ok, R.id.text_cancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.text_ok:
                String selectedReason = "";
                Intent intent = new Intent();
                intent.putExtra("reasonCode", mSelectedReasonCode);
                if (mSelectedReasonCode.equalsIgnoreCase("605")) {
                    selectedReason = mEditOther.getText().toString().trim();

                } else {
                    int index = Integer.parseInt(mSelectedReasonCode) - 601;
                    selectedReason = getResources().getStringArray(R.array.decline_reasons_description)[index];
                }
                intent.putExtra("data", selectedReason);
                getTargetFragment().onActivityResult(NccConstants.DISPATCHER_DECLINE_JOB, Activity.RESULT_OK, intent);
                dismiss();
                break;
            case R.id.text_cancel:
                dismiss();
                break;
        }
    }

}
