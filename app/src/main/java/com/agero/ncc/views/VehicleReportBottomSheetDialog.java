package com.agero.ncc.views;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.activities.HomeActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VehicleReportBottomSheetDialog extends BottomSheetDialogFragment {
    HomeActivity mHomeActivity;
    VehicleReportClickListener vehicleReportClickListener;


    @BindView(R.id.text_view_vehicle)
    TextView mTextViewVehicle;
    @BindView(R.id.text_remove_vehicle)
    TextView mTextRemoveVehicle;

    public static VehicleReportBottomSheetDialog getInstance() {
        return new VehicleReportBottomSheetDialog();
    }

    public void setVehicleReportClickListener(VehicleReportClickListener vehicleReportClickListener) {
        this.vehicleReportClickListener = vehicleReportClickListener;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_vehicle_report_bottom_sheet, container, false);
        ButterKnife.bind(this, view);
        mHomeActivity = (HomeActivity) getActivity();
        return view;
    }

    @OnClick({R.id.text_view_vehicle, R.id.text_remove_vehicle})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.text_view_vehicle:
                vehicleReportClickListener.onClickView();
                break;
            case R.id.text_remove_vehicle:
                vehicleReportClickListener.onClickRemove();
                break;
        }
    }


    public interface VehicleReportClickListener {

        public void onClickView();

        public void onClickRemove();


    }
}
