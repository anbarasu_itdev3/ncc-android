package com.agero.ncc.views;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.activities.WelcomeActivity;
import com.agero.ncc.fragments.CameraBaseFragment;
import com.agero.ncc.utils.UserError;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class AddProfileBottomSheetDialog extends BottomSheetDialogFragment {
    UserError mUserError;
    SharProfileDialogClickListener sharProfileDialogClickListener;
    @BindView(R.id.image_delete_photo)
    ImageView mImageDeletePhoto;
    @BindView(R.id.image_add_video)
    ImageView mImageAddVideo;
    @BindView(R.id.text_delete_photo)
    TextView mTextDeletePhoto;
    @BindView(R.id.text_label_add_profile)
    TextView mTextLabelAddProfile;
    @BindView(R.id.text_add_video)
    TextView mTextAddVideo;

    @BindView(R.id.text_add_photo)
    TextView mTextAddPhoto;

    @BindView(R.id.text_choose_photo)
    TextView mTextChoosePhoto;

    public static AddProfileBottomSheetDialog getInstance() {
        return new AddProfileBottomSheetDialog();
    }

    public void setSharDialogClickListener(SharProfileDialogClickListener sharProfileDialogClickListener) {
        this.sharProfileDialogClickListener = sharProfileDialogClickListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_profile_bottom_sheet, container, false);
        ButterKnife.bind(this, view);
        mUserError = new UserError();
        int bottomSheetScreen = 0;
        if (getArguments() != null) {
            bottomSheetScreen = getArguments().getInt(CameraBaseFragment.BUNDLE_KEY);
        }
        if (bottomSheetScreen == CameraBaseFragment.SHOW_BOTTOM_SHEET_CREATE_ACCOUNT) {
            mTextDeletePhoto.setVisibility(View.GONE);
            mImageDeletePhoto.setVisibility(View.GONE);
            mTextAddVideo.setVisibility(View.GONE);
            mImageAddVideo.setVisibility(View.GONE);
            mTextLabelAddProfile.setText(R.string.profile_label_add_profile);
        } else if (bottomSheetScreen == CameraBaseFragment.SHOW_BOTTOM_SHEET_BASIC_DETAILS) {
            mTextLabelAddProfile.setText(R.string.basic_label_edit_profile);
            mTextAddVideo.setVisibility(View.GONE);
            mImageAddVideo.setVisibility(View.GONE);
        } else if (bottomSheetScreen == CameraBaseFragment.SHOW_BOTTOM_SHEET_VEHICLE_CONDITION_PHOTO) {
            mTextAddPhoto.setText(R.string.take_photo);
            mTextChoosePhoto.setText(R.string.choose_photos);
            mTextAddVideo.setText(R.string.take_video);
            mTextAddVideo.setVisibility(View.VISIBLE);
            mImageAddVideo.setVisibility(View.VISIBLE);
            mTextLabelAddProfile.setVisibility(View.GONE);
            mTextDeletePhoto.setVisibility(View.GONE);
            mImageDeletePhoto.setVisibility(View.GONE);
        }

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                BottomSheetDialog dialog = (BottomSheetDialog) getDialog();
                FrameLayout bottomSheet = (FrameLayout)
                        dialog.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                behavior.setPeekHeight(0);
            }
        });
    }
    @OnClick({R.id.text_add_photo, R.id.text_choose_photo, R.id.text_delete_photo, R.id.text_add_video})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.text_add_photo:
//                if (getArguments() != null && getArguments().getInt(CameraBaseFragment.BUNDLE_KEY) == CameraBaseFragment.SHOW_BOTTOM_SHEET_VEHICLE_CONDITION_PHOTO) {
//                    sharProfileDialogClickListener.onTakePhotoVideoClick();
//                }else{
                    sharProfileDialogClickListener.onTakePhotoClick();
//                }
                break;
            case R.id.text_add_video:
                sharProfileDialogClickListener.onTakeVideoClick();
                break;
            case R.id.text_choose_photo:
                sharProfileDialogClickListener.onChoosePhotoClick();
                break;
            case R.id.text_delete_photo:
                sharProfileDialogClickListener.onDeletePhotoClick();
                break;

        }
    }


    public interface SharProfileDialogClickListener {

        public void onTakePhotoClick();

        public void onTakeVideoClick();

        public void onChoosePhotoClick();

        public void onDeletePhotoClick();

//        public void onTakePhotoVideoClick();
    }

}
