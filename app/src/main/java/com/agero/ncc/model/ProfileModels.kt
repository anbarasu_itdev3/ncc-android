package com.agero.ncc.model

import android.media.session.MediaSession


data class Profile(
		var userId: String? = null,
		var Equipment: Equipment?=null,
		var email: String? = null,// lsantos@lowesttowing.com
		var firstName: String? = null,// Lester
		var lastName: String? = null,// Santos
		var mobilePhoneNumber: String? = null,// 5555557834
		var password: String? = null,// 1234567
		var imageUrl: String? = null,
		var roles: List<String>? = null,
		var onDuty: Boolean? = false,
		var status : String? = null,
		var location: Location? = null,
		var backgroundCheckStatus: String? = null
)

data class Location(var latitude: Double? = null, //41.7007728
					var longitude: Double? = null //-71.4539157
)