package com.agero.ncc.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.model.JobDetail;
import com.agero.ncc.model.Profile;
import com.agero.ncc.utils.DateTimeUtils;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.UiUtils;
import com.agero.ncc.utils.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import org.w3c.dom.Text;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderAdapter;
import de.hdodenhof.circleimageview.CircleImageView;
import io.netopen.hotbitmapgg.library.view.RingProgressBar;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.agero.ncc.utils.DateTimeUtils.getTimeDifference;

public class DispatcherJobsAdapter extends RecyclerView.Adapter<DispatcherJobsAdapter.ViewHolder>
        implements StickyHeaderAdapter<DispatcherJobsAdapter.HeaderHolder> {


    private Context mContext;
    private ArrayList<JobDetail> jobList;
    private int[] mJobTypeCountList;

    private HashMap<String, Profile> mDriverProfile = new HashMap<>();

    private DatabaseReference myProfileImageRef;

    private String mVendorId = "";

    public static final int JOB_DISPLAY_TIME_LIMIT_SEC = 60 * 60;

    public Disposable mDisposable;

    private int mSelectedJobPosition;

    private HashMap<Integer, TimerTagging> mOfferedJobTimerHash = null;

    private long mLatestJobStartTime = JOB_DISPLAY_TIME_LIMIT_SEC;

    public DispatcherJobsAdapter(Context mContext, ArrayList<JobDetail> statuses,
                                 int[] jobTypeCountList, String vendorId, int selectedJobPosition) {
        this.mContext = mContext;
        this.jobList = statuses;
        this.mJobTypeCountList = jobTypeCountList;
        this.mVendorId = vendorId;
        this.mSelectedJobPosition = selectedJobPosition;

        if (jobList != null && jobList.size() > 0) {
            for (int i = 0; i < mJobTypeCountList[0]; i++) {
                if (NccConstants.JOB_STATUS_OFFERED.equalsIgnoreCase(jobList.get(i).getCurrentStatus().getStatusCode()) && !TextUtils.isEmpty(jobList.get(i).getDispatchCreatedDate())) {
                    long timeDifference = getTimeDifference(jobList.get(i).getDispatchCreatedDate());
                    if (mLatestJobStartTime > timeDifference && timeDifference < JOB_DISPLAY_TIME_LIMIT_SEC) {
                        mLatestJobStartTime = timeDifference;
                    }
                }
            }

            createTimer();
        }

    }

    private void createTimer() {
        mDisposable = Observable.intervalRange(mLatestJobStartTime, (JOB_DISPLAY_TIME_LIMIT_SEC - mLatestJobStartTime) + 1, 0,
                1, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(aLong -> {
                    if (mOfferedJobTimerHash != null) {
                        ArrayList<Integer> removeKeys = null;
                        for (Map.Entry<Integer, TimerTagging> entry :
                                mOfferedJobTimerHash.entrySet()) {
                            long sec = (aLong + entry.getValue().timeDifference) % 60;
                            long min = ((aLong + entry.getValue().timeDifference) / 60) % 60;

                            String text = String.format("%02d:%02d", min, sec);
                            if (text.equalsIgnoreCase("00:00")) {
                                text = "60:00";
                                if (removeKeys == null) {
                                    removeKeys = new ArrayList<>();
                                }
                                removeKeys.add(entry.getKey());
                            }
                            entry.getValue().timerTv.setText(text);

                        }
                        if (removeKeys != null) {
                            for (Integer pos : removeKeys) {
                                mOfferedJobTimerHash.remove(pos);
                            }
                        }
                    }
                })
                .doOnComplete(() -> {
                    if (mOfferedJobTimerHash != null) {
                        for (Map.Entry<Integer, TimerTagging> entry :
                                mOfferedJobTimerHash.entrySet()) {
                            entry.getValue().timerTv.setText("60:00");
                        }
                        mOfferedJobTimerHash.clear();
                        mOfferedJobTimerHash = null;
                    }

                    mDisposable.dispose();
                })
                .subscribe();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        final View view = LayoutInflater.from(mContext)
                .inflate(R.layout.dispatcher_jobs_child_item, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        if (mContext.getResources().getBoolean(R.bool.isTablet)) {
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mSelectedJobPosition = position;
                    notifyDataSetChanged();
                }
            });

            if (mSelectedJobPosition == position) {
                viewHolder.itemView.setBackgroundColor(mContext.getResources().getColor(R.color.tab_tap_color));
            } else {
                viewHolder.itemView.setBackgroundColor(mContext.getResources().getColor(R.color.ncc_white));
            }
        }
        JobDetail job = jobList.get(viewHolder.getAdapterPosition());
        if (job.getServiceTypeCode() != null) {
            StringBuilder jobTitle = new StringBuilder(Utils.getDisplayServiceTypes(mContext, job.getServiceTypeCode()));
            jobTitle.append(" - ");
            jobTitle.append(job.getVehicle().getYear());
            jobTitle.append(" ");
            jobTitle.append(job.getVehicle().getMake());
            jobTitle.append(" ");
            jobTitle.append(job.getVehicle().getModel());
            jobTitle.append(" ");
            jobTitle.append(job.getVehicle().getColor());
            viewHolder.jobTitle.setText(jobTitle);

            StringBuilder jobAddress =
                    new StringBuilder(job.getDisablementLocation().getAddressLine1());
            jobAddress.append(", ");
            jobAddress.append(job.getDisablementLocation().getCity());
            jobAddress.append(", ");
            jobAddress.append(job.getDisablementLocation().getState());
            jobAddress.append(", ");
            jobAddress.append(job.getDisablementLocation().getPostalCode());
            viewHolder.jobAddress.setText(jobAddress);
            viewHolder.jobStatus.setTextColor(
                    mContext.getResources().getColor(R.color.ncc_gray_666));
            viewHolder.mImageProfileTv.setVisibility(View.GONE);
            viewHolder.mImageProfile.setVisibility(View.INVISIBLE);
            viewHolder.mTimerTv.setVisibility(View.GONE);
            viewHolder.mRingProgressBar.setVisibility(View.GONE);
            viewHolder.mRingProgressBar.setRingColor(R.color.progress_red_color);
            viewHolder.mPendingTv.setVisibility(View.GONE);
            viewHolder.jobStatus.setVisibility(View.VISIBLE);
            viewHolder.jobEta.setVisibility(View.VISIBLE);
            viewHolder.mImageOffer.setVisibility(View.GONE);
            viewHolder.mImageAwarded.setVisibility(View.GONE);
            if (job != null && job.getDispatchId() != null) {
                viewHolder.mTextJobId.setText("Job #" + UiUtils.getJobIdDisplayFormat(job.getDispatchId()));
            }

            switch (job.getCurrentStatus()
                    .getStatusCode()) {
                case NccConstants.JOB_STATUS_OFFERED: {
                    viewHolder.mTimerTv.setVisibility(View.VISIBLE);
                    viewHolder.mImageProfile.setVisibility(View.INVISIBLE);

                    viewHolder.jobStatus.setText("$" + job.getInvoiceAmount());

                    viewHolder.mImageOffer.setVisibility(View.VISIBLE);
                    viewHolder.jobStatus.setVisibility(View.GONE);

                    if (job.getOfferedToFacilitiesCount() == 1) {
//                    viewHolder.jobEta.setText(
//                            mContext.getString(R.string.dot) + " Exclusive Offer");
                        viewHolder.mImageOffer.setVisibility(View.VISIBLE);
                        viewHolder.mImageProfile.setBackgroundResource(R.drawable.circular_tv_bg_grey);
                        viewHolder.mTimerTv.setTextColor(mContext.getResources().getColor(R.color.ncc_black));
                        viewHolder.mTimerTv.setBackgroundResource(R.drawable.circular_tv_bg_grey);
                        viewHolder.jobEta.setText("Exclusive offer (for 1 minute)");
                        viewHolder.mImageOffer.setImageResource(R.drawable.ic_offer_exclusive);
                    } else if (job.getOfferedToFacilitiesCount() > 1) {
//                    viewHolder.jobEta.setText(
//                            mContext.getString(R.string.dot) + " Offered to " + jobList.get(
//                                    position).getOfferedToFacilitiesCount() + " partners");
                        viewHolder.mImageOffer.setImageResource(R.drawable.ic_offer_nonexclusive);
                        viewHolder.mImageProfile.setBackgroundResource(R.drawable.circular_tv_bg_pink);
                        viewHolder.mTimerTv.setBackgroundResource(R.drawable.circular_tv_bg_pink);
                        viewHolder.mTimerTv.setTextColor(mContext.getResources().getColor(R.color.jobdetail_status_info_cancel_text_green));
                        viewHolder.jobEta.setText("Offered to other providers");
                    }
                    if (job.getOfferAcceptedByFacilitiesCount() > 0) {
                        viewHolder.mImageOffer.setImageResource(R.drawable.ic_offer_nonexclusive);
                        viewHolder.mImageProfile.setBackgroundResource(R.drawable.circular_tv_bg_pink);
                        viewHolder.mTimerTv.setTextColor(mContext.getResources().getColor(R.color.jobdetail_status_info_cancel_text_green));
                        if (job.getOfferAcceptedByFacilitiesCount() == 1 && job.getOfferedToFacilitiesCount() == 1) {
//                        viewHolder.jobEta.setText(
//                                mContext.getString(R.string.dot) + " Offer requested");
                            viewHolder.jobEta.setText("Offered to other providers");
                        } else {
//                        viewHolder.jobEta.setText(mContext.getString(R.string.dot)
//                                + " "
//                                + job.getOfferAcceptedByFacilitiesCount()
//                                + " of "
//                                + job.getOfferedToFacilitiesCount()
//                                + " partners requested");
                            viewHolder.mImageOffer.setImageResource(R.drawable.ic_offer_nonexclusive);
                            viewHolder.mImageProfile.setBackgroundResource(R.drawable.circular_tv_bg_pink);
                            viewHolder.mTimerTv.setTextColor(mContext.getResources().getColor(R.color.jobdetail_status_info_cancel_text_green));
                            viewHolder.jobEta.setText("Offered to other providers");
                        }
                    }
                    //viewHolder.mImageProfile.setImageResource(R.drawable.ic_add_grey);
                    viewHolder.mTimerTv.setTag(viewHolder.getAdapterPosition());
                    viewHolder.mTimerTv.setVisibility(View.VISIBLE);
//                viewHolder.mRingProgressBar.setVisibility(View.VISIBLE);
//                viewHolder.mRingProgressBar.setRingColor(mContext.getResources()
//                        .getColor(R.color.progress_red_color));
                    //viewHolder.mRingProgressBar.setRingProgressColor(R.color.progress_red_color);


                    if (job.getDispatchCreatedDate() != null) {
                        long startTime = getTimeDifference(job.getDispatchCreatedDate().toString());
                        if (startTime < JOB_DISPLAY_TIME_LIMIT_SEC) {

                            addJobToTimer(position, viewHolder.mTimerTv, getTimeDifference(getTimeDifference(job.getDispatchCreatedDate().toString()), this.mLatestJobStartTime));

                        } else {
                            viewHolder.mTimerTv.setText("60:00");
                        }
                    }

                }
                break;
                case NccConstants.JOB_STATUS_ACCEPTED: {

                    viewHolder.mTimerTv.setTag(viewHolder.getAdapterPosition());
                    viewHolder.mTimerTv.setVisibility(View.INVISIBLE);
                    viewHolder.mRingProgressBar.setVisibility(View.VISIBLE);
                    viewHolder.mRingProgressBar.setRingColor(mContext.getResources()
                            .getColor(R.color.ncc_white));
                    viewHolder.mRingProgressBar.setRingProgressColor(mContext.getResources()
                            .getColor(R.color.jobdetail_message_color));
                    viewHolder.mPendingTv.setVisibility(View.VISIBLE);
                    viewHolder.jobStatus.setVisibility(View.GONE);
                    viewHolder.jobEta.setVisibility(View.GONE);
                    viewHolder.mRingProgressBar.setProgress(70);
                    viewHolder.mPendingTv.setText(mContext.getString(R.string.pending_approval));
                    viewHolder.mPendingTv.setTextColor(mContext.getResources()
                            .getColor(R.color.jobdetail_message_color));
                }
                break;
                case NccConstants.JOB_STATUS_AWARDED: {
                    viewHolder.mTimerTv.setTag(viewHolder.getAdapterPosition());
                    viewHolder.mTimerTv.setVisibility(View.INVISIBLE);
                    viewHolder.mImageAwarded.setVisibility(View.VISIBLE);
                    viewHolder.mRingProgressBar.setProgress(100);
                    viewHolder.mRingProgressBar.setVisibility(View.VISIBLE);
                    viewHolder.mRingProgressBar.setRingColor(mContext.getResources()
                            .getColor(R.color.ncc_white));
                    viewHolder.mRingProgressBar.setRingProgressColor(mContext.getResources()
                            .getColor(R.color.jobdetail_background_button_green));
                    viewHolder.mPendingTv.setVisibility(View.VISIBLE);
                    viewHolder.jobStatus.setVisibility(View.GONE);
                    viewHolder.jobEta.setVisibility(View.GONE);
                    viewHolder.mPendingTv.setText(mContext.getString(R.string.job_awarded));
                    viewHolder.mPendingTv.setTextColor(mContext.getResources()
                            .getColor(R.color.jobdetail_background_button_green));
                }
                break;
                case NccConstants.JOB_STATUS_UNASSIGNED: {
                    viewHolder.mImageProfile.setVisibility(View.VISIBLE);
                    viewHolder.jobStatus.setText(Utils.getDisplayStatusText(mContext, job.getCurrentStatus()
                            .getStatusCode()));
                    viewHolder.jobStatus.setTextColor(mContext.getResources()
                            .getColor(R.color.dispatcher_jobdetail_not_assigned_color));
                    String etaDateTime = "";
                    if (job != null) {
                        etaDateTime = getETADateTime(job);
                    }
                    if (TextUtils.isEmpty(etaDateTime)) {
                        viewHolder.jobEta.setText(mContext.getString(R.string.dot) + " " + mContext.getString(R.string.not_available));
                    } else {
                        viewHolder.jobEta.setText(mContext.getString(R.string.dot) + " " + etaDateTime);
                    }

//                    viewHolder.jobEta.setText(mContext.getString(R.string.dot)
//                            + " ETA "
//                            + DateTimeUtils.getDisplayTimeStamp(new Date()));

                    viewHolder.mImageProfile.setImageResource(R.drawable.ic_add_grey);
                }
                break;
                default: {
                    viewHolder.mImageProfile.setVisibility(View.VISIBLE);
                    if (job.getCurrentStatus().getIsPending() != null && job.getCurrentStatus().getIsPending()) {
                        viewHolder.jobStatus.setText(mContext.getString(R.string.text_pending) + " " + Utils.getDisplayStatusText(mContext, job.getCurrentStatus().getStatusCode()));
                        viewHolder.jobStatus.setTextColor(mContext.getResources()
                                .getColor(R.color.jobdetail_pending_color));
                    } else if (job.getCurrentStatus().getIsJobOnHold() != null && job.getCurrentStatus().getIsJobOnHold()) {
                        viewHolder.jobStatus.setText(mContext.getString(R.string.status_on_hold));
                        viewHolder.jobStatus.setTextColor(mContext.getResources()
                                .getColor(R.color.jobdetail_pending_color));
                    } else {
                        viewHolder.jobStatus.setTextColor(mContext.getResources()
                                .getColor(R.color.jobdetail_heading_color));
                        viewHolder.jobStatus.setText(Utils.getDisplayStatusText(mContext, job.getCurrentStatus().getStatusCode()));
                    }

                    String etaDateTime = "";
                    if (job != null) {
                        etaDateTime = getETADateTime(job);
                    }
                    if (TextUtils.isEmpty(etaDateTime)) {
                        viewHolder.jobEta.setText(mContext.getString(R.string.dot) + " " + mContext.getString(R.string.not_available));
                    } else {
                        viewHolder.jobEta.setText(mContext.getString(R.string.dot) + " " + etaDateTime);
                    }


//                    viewHolder.jobEta.setText(mContext.getString(R.string.dot)
//                            + " ETA "
//                            + DateTimeUtils.getDisplayTimeStamp(new Date()));
                    String driverImageUrl = "";

                    if (!TextUtils.isEmpty(job.getDispatchAssignedToName())) {
                        String name = "";
                        String driverName = job.getDispatchAssignedToName();
                        String[] driverNameStr = driverName.split("\\s+");
                        name = String.valueOf(driverNameStr[0].charAt(0));
                        if (!TextUtils.isEmpty(driverNameStr[1])) {
                            name += String.valueOf(driverNameStr[1].charAt(0));
                        }
                        if (!TextUtils.isEmpty(name)) {
                            name.toUpperCase(Locale.ENGLISH);
                        }
                        viewHolder.mImageProfileTv.setText(name);
                        viewHolder.mImageProfileTv.setVisibility(View.VISIBLE);
                    } else if (mDriverProfile.containsKey(job.getDispatchAssignedToId())) {
                        driverImageUrl = mDriverProfile.get(job.getDispatchAssignedToId()).getImageUrl();
                        String name = "";
                        if (!TextUtils.isEmpty(mDriverProfile.get(job.getDispatchAssignedToId()).getFirstName())) {
                            name = String.valueOf(
                                    mDriverProfile.get(job.getDispatchAssignedToId()).getFirstName().charAt(0));
                        }
                        if (!TextUtils.isEmpty(mDriverProfile.get(job.getDispatchAssignedToId()).getLastName())) {
                            name += String.valueOf(
                                    mDriverProfile.get(job.getDispatchAssignedToId()).getLastName().charAt(0));
                        }
                        if (!TextUtils.isEmpty(name)) {
                            name.toUpperCase(Locale.ENGLISH);
                        }

//                        String name = (String.valueOf(
//                            mDriverProfile.get(job.getDispatchAssignedToId()).getFirstName().charAt(0)) + String
//                            .valueOf(mDriverProfile.get(job.getDispatchAssignedToId()).getLastName().charAt(0)))
//                            .toUpperCase(Locale.ENGLISH);
                        viewHolder.mImageProfileTv.setText(name);
                        viewHolder.mImageProfileTv.setVisibility(View.VISIBLE);
                    } else {
                        getProfileImageUrl(job.getDispatchAssignedToId());
                    }

                    if (!TextUtils.isEmpty(driverImageUrl)) {

                        Glide.with(mContext)
                                .load(driverImageUrl)
                                .listener(new RequestListener<Drawable>() {
                                    @Override
                                    public boolean onLoadFailed(@Nullable GlideException e,
                                                                Object model, Target<Drawable> target,
                                                                boolean isFirstResource) {
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(Drawable resource, Object model,
                                                                   Target<Drawable> target, DataSource dataSource,
                                                                   boolean isFirstResource) {
                                        viewHolder.mImageProfile.setVisibility(View.VISIBLE);
                                        viewHolder.mImageProfileTv.setVisibility(View.GONE);
                                        return false;
                                    }
                                })
                                .into(viewHolder.mImageProfile);
                    }
                }
                break;

            }
            viewHolder.item.setMaxHeight(500);
        } else {
            viewHolder.item.setMaxHeight(0);
        }
    }


    private String getETADateTime(JobDetail job) {
        try {
            if (!TextUtils.isEmpty(job.getEtaDateTime())) {
                String dateTime;
                if (job.getEtaExtensionCount() == null || job.getEtaExtensionCount() == 0) {
                    dateTime = DateTimeUtils.getEtaDateTime(job.getEtaDateTime(), job.getQuotedEta(), false, job.getDisablementLocation().getTimeZone());
                } else {
                    dateTime = DateTimeUtils.getEtaDateTime(job.getEtaDateTime(), job.getTotalEtaInMinutes(), false, job.getDisablementLocation().getTimeZone());
                }

                if (dateTime != null && dateTime.contains("am")) {
                    dateTime = dateTime.replace("am", "AM");
                }
                if (dateTime != null && dateTime.contains("pm")) {
                    dateTime = dateTime.replace("pm", "PM");
                }
                return dateTime;
            } else if (!TextUtils.isEmpty(job.getAcceptedDateTime())) {
                String dateTime;
                if (job.getEtaExtensionCount() == null || job.getEtaExtensionCount() == 0) {
                    dateTime = DateTimeUtils.getEtaDateTime(job.getAcceptedDateTime(), job.getQuotedEta(), true, job.getDisablementLocation().getTimeZone());
                } else {
                    dateTime = DateTimeUtils.getEtaDateTime(job.getAcceptedDateTime(), job.getTotalEtaInMinutes(), true, job.getDisablementLocation().getTimeZone());
                }

                if (dateTime != null && dateTime.contains("am")) {
                    dateTime = dateTime.replace("am", "AM");
                }
                if (dateTime != null && dateTime.contains("pm")) {
                    dateTime = dateTime.replace("pm", "PM");
                }
                return dateTime;
            } else {
                return "";
            }
        } catch (ParseException ex) {

        }
        return null;
    }

    private void addJobToTimer(int position, TextView timerTv, long diffTimeSec) {
        if (mOfferedJobTimerHash == null) {
            mOfferedJobTimerHash = new HashMap<>();
        }
        if (mOfferedJobTimerHash.containsKey(position)) {
            TimerTagging timerTagging = mOfferedJobTimerHash.get(position);
            timerTagging.timerTv = timerTv;
            mOfferedJobTimerHash.put(position, timerTagging);
        } else {
            mOfferedJobTimerHash.put(position, new TimerTagging(timerTv, diffTimeSec));
        }


    }

    private ValueEventListener mProfileImageValueEventListner = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (dataSnapshot.hasChildren()) {
                try {

                    Profile userProfile = dataSnapshot.getValue(Profile.class);
                    if (userProfile != null) {
                        HashMap<String, String> extraDatas = new HashMap<>();
                        extraDatas.put("json", new Gson().toJson(userProfile));
                        ((HomeActivity) mContext).mintlogEventExtraData("Dispatcher Jobs List Driver Profile Json", extraDatas);
                        mDriverProfile.put(dataSnapshot.getKey(), userProfile);
                        notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    ((HomeActivity) mContext).mintLogException(e);
                }
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            if (databaseError != null && databaseError.getMessage() != null && !((HomeActivity) mContext).isFinishing()) {
                ((HomeActivity) mContext).mintlogEvent("Profile Image Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
            }
        }
    };

    private void getProfileImageUrl(String driverId) {

        if (!TextUtils.isEmpty(driverId) && !mDriverProfile.containsKey(driverId)) {
            getProfileImage(driverId);

        }
    }

    private void getProfileImage(String driverId) {
        TokenManager.getInstance().validateToken(((HomeActivity) mContext), new TokenManager.TokenReponseListener() {
            @Override
            public void onRefreshSuccess() {
                myProfileImageRef = FirebaseDatabase.getInstance()
                        .getReference("Users/" + mVendorId + "/" + driverId + "/");
                //myProfileImageRef.keepSynced(true);
                myProfileImageRef.addListenerForSingleValueEvent(mProfileImageValueEventListner);
            }

            @Override
            public void onRefreshFailure() {
                ((HomeActivity) mContext).tokenRefreshFailed();
            }
        });
    }

    public void removeValueEventListners() {
        try {
            if (myProfileImageRef != null) {
                myProfileImageRef.removeEventListener(mProfileImageValueEventListner);
            }
        } catch (Exception e) {

        }
    }

    @Override
    public int getItemCount() {

        return jobList.size();
    }

    @Override
    public long getHeaderId(int position) {
        JobDetail jobDetail = jobList.get(position);
        if (jobDetail.getCurrentStatus()
                .getStatusCode()
                .equalsIgnoreCase(NccConstants.JOB_STATUS_OFFERED) || NccConstants.JOB_STATUS_ACCEPTED.equalsIgnoreCase(jobDetail.getCurrentStatus()
                .getStatusCode()) || NccConstants.JOB_STATUS_AWARDED.equalsIgnoreCase(jobDetail.getCurrentStatus()
                .getStatusCode())) {
            return 0;
        } else if (jobDetail.getCurrentStatus()
                .getStatusCode()
                .equalsIgnoreCase(NccConstants.JOB_STATUS_UNASSIGNED)) {
            return 1;
        } else {
            return 2;
        }
    }

    @Override
    public HeaderHolder onCreateHeaderViewHolder(ViewGroup parent) {
        final View view = LayoutInflater.from(mContext)
                .inflate(R.layout.dispatcher_jobs_header_item, parent, false);
        return new HeaderHolder(view);
    }

    @Override
    public void onBindHeaderViewHolder(HeaderHolder viewholder, int position) {
        int headerPosition = (int) getHeaderId(position);
        viewholder.jobType.setText(NccConstants.DISPATCHER_HEADERS[headerPosition]);
        viewholder.jobCount.setText(" " + mJobTypeCountList[headerPosition]);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        public ConstraintLayout item;
        @BindView(R.id.job_title)
        public TextView jobTitle;
        @BindView(R.id.job_address)
        public TextView jobAddress;
        @BindView(R.id.job_status)
        public TextView jobStatus;
        @BindView(R.id.job_eta)
        public TextView jobEta;
        @BindView(R.id.image_profile_tv)
        public TextView mImageProfileTv;
        @BindView(R.id.image_profile)
        CircleImageView mImageProfile;
        @BindView(R.id.text_timer_min)
        TextView mTimerTv;
        @BindView(R.id.ring_progress_bar)
        RingProgressBar mRingProgressBar;
        @BindView(R.id.job_pending)
        TextView mPendingTv;
        @BindView(R.id.text_job_id)
        TextView mTextJobId;
        @BindView(R.id.image_offer)
        ImageView mImageOffer;
        @BindView(R.id.image_job_awarded)
        ImageView mImageAwarded;
        @BindView(R.id.constraint_disablement)
        ConstraintLayout mConstraintDisablement;

        public ViewHolder(View itemView) {
            super(itemView);

            item = (ConstraintLayout) itemView;
            ButterKnife.bind(this, itemView);
        }
    }

    static class HeaderHolder extends RecyclerView.ViewHolder {
        public ConstraintLayout header;
        @BindView(R.id.job_type)
        public TextView jobType;
        @BindView(R.id.job_count)
        public TextView jobCount;

        public HeaderHolder(View itemView) {
            super(itemView);

            header = (ConstraintLayout) itemView;
            ButterKnife.bind(this, itemView);
        }
    }

    static class TimerTagging {
        TextView timerTv;
        long timeDifference;

        public TimerTagging(TextView timerTv, long diffTimeSec) {
            this.timerTv = timerTv;
            this.timeDifference = diffTimeSec;
        }
    }
}
