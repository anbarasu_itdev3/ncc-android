package com.agero.ncc.adapter;


import android.content.Context;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.model.VehicleReportModelEntity;
import com.agero.ncc.utils.FileUtils;
import com.agero.ncc.utils.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Adapter for VehicleConditionPhotoFragment.java
 */
public class VehiclePhotoAdapter extends RecyclerView.Adapter<VehiclePhotoAdapter.ItemViewHolder> {
    Context mContext;
    ArrayList<VehicleReportModelEntity> mPhotoUriList;

    public VehiclePhotoAdapter(Context mContext, ArrayList<VehicleReportModelEntity> mPhotoUriList) {
        this.mContext = mContext;
        this.mPhotoUriList = mPhotoUriList;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(mContext)
                .inflate(R.layout.vehicle_condition_photo_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, int position) {

        VehicleReportModelEntity vehicleReportModelEntity = (VehicleReportModelEntity) mPhotoUriList.get(position);
        holder.mTextViewDuration.setVisibility(View.INVISIBLE);

        if(position==0){
            holder.loader.setVisibility(View.GONE);
        }else{
            holder.loader.setVisibility(View.VISIBLE);

        }

        if (vehicleReportModelEntity.getLocalUri() != null) {

            String uri = vehicleReportModelEntity.getLocalUri().getPath();

            if (FileUtils.getFileType(uri) == FileUtils.FileType.PDF) {
                Glide.with(mContext)
                        .load(Utils.generateImageFromPdf(mContext,
                                vehicleReportModelEntity.getLocalUri()))
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                holder.loader.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                holder.loader.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(holder.mImageView);
            } else if (FileUtils.getFileType(uri) == FileUtils.FileType.VIDEO) {
                String selectedVideoFilePath = FileUtils.getPath(mContext, vehicleReportModelEntity.getLocalUri());
                MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                retriever.setDataSource(selectedVideoFilePath); // Enter Full File Path Here
                String time = retriever.extractMetadata(
                        MediaMetadataRetriever.METADATA_KEY_DURATION);
                long timeInmillisec = Long.parseLong(time);
                double minutes = (timeInmillisec / (1000.0 * 60)) % 60;

                holder.mTextViewDuration.setText(
                        String.format(Locale.getDefault(), "%.2f", minutes));
                Glide.with(mContext)
                        .load(vehicleReportModelEntity.getLocalUri())
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                holder.loader.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                holder.loader.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(holder.mImageView);
                holder.mTextViewDuration.setVisibility(View.VISIBLE);

            } else {
                Glide.with(mContext)
                        .load(vehicleReportModelEntity.getLocalUri())
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                holder.loader.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                holder.loader.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(holder.mImageView);
            }

        } else if (vehicleReportModelEntity.getDocUrl() != null) {


            holder.mTextViewDuration.setVisibility(View.INVISIBLE);

            if (vehicleReportModelEntity.getType().equalsIgnoreCase("pdf")) {
                Glide.with(mContext)
                        .load(vehicleReportModelEntity.getThumbUrl())
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                holder.loader.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                holder.loader.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(holder.mImageView);
            } else if (vehicleReportModelEntity.getType().equalsIgnoreCase("video")) {

                holder.mTextViewDuration.setVisibility(View.VISIBLE);


                if(vehicleReportModelEntity.getVideoLength() != null) {
                    double minutes = (vehicleReportModelEntity.getVideoLength() / (1000.0 * 60)) % 60;
                    holder.mTextViewDuration.setText(
                            String.format(Locale.getDefault(), "%.2f", minutes));
                }
                Glide.with(mContext)
                        .load(vehicleReportModelEntity.getThumbUrl())
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                holder.loader.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                holder.loader.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(holder.mImageView);
            } else {
                Glide.with(mContext)
                        .load(vehicleReportModelEntity.getDocUrl())
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                holder.loader.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                holder.loader.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(holder.mImageView);
            }
        } else {
            if(mPhotoUriList.size() >=13){
                holder.mImageView.setAlpha(0.4f);
            }else{
                holder.mImageView.setAlpha(1f);
            }
            holder.mImageView.setImageResource(R.drawable.ic_ico_camera);
        }
    }

    @Override
    public int getItemCount() {
        return mPhotoUriList.size();
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {

        //        @BindView(R.id.image_add_photos) ImageView mImageAddPhotos;
        @BindView(R.id.image_view_photo)
        ImageView mImageView;
        @BindView(R.id.view_duration)
        TextView mTextViewDuration;
        @BindView(R.id.loader)
        ProgressBar loader;

        ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
