package com.agero.ncc.services;

import android.content.SharedPreferences;

import com.agero.ncc.app.AppModule;
import com.agero.ncc.model.PushToken;
import com.agero.ncc.utils.NccConstants;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import javax.inject.Inject;

public class NccFirebaseInstanceIdService extends FirebaseInstanceIdService {

    @Inject
    SharedPreferences mPrefs;

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        mPrefs = getSharedPreferences(AppModule.NCC_PREFS, MODE_PRIVATE);
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {
        if (token != null && !mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "").isEmpty()) {
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference reference = database.getReference("Users/"+mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID,"")
                            +"/"+mPrefs.getString(NccConstants.SIGNIN_USER_ID, "")+"/token");
            //reference.keepSynced(true);
            PushToken pushToken = new PushToken(token, "android");
            reference.push().setValue(pushToken);
            mPrefs.edit().putString(NccConstants.PREF_KEY_REGISTRATION_TOKEN, token).apply();
        }
    }
}
